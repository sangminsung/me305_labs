# -*- coding: utf-8 -*-
"""
@file task_user6.py
@brief   A Task User file that delegates the user inputed values
@details This file is used to implement a simple data collection interface, start and stop the balancer, and end the program as the user prompts. 
         The user input values can be UPPER or lower case and the user is given the set of user commands as the follows:

         * b or B : Engage balancing the plateform
         * q or Q : Disengage balancing the plateform 
         * g or G : Start collecting data of the ball position & velocity
         * s or S : End data collection prematurely
         * e or E : end the program
         * c or C : Clear a extrernal interupt fault        

@image html  task_user_final.jpg "Finite State Space Diagram" width=600px

@author Sangmin Sung
@author Travis Welch
@date October 14, 2021
"""
import pyb 

# @brief A variable that represents the Vertual COM port.
# @details This variable is used for understanding and taking in the user key commands.
#
serport = pyb.USB_VCP()

S0_INIT = 0  # Initial state
S1_Ball_Balancing = 1  # "z"- Zero the position of the encoder 1
S2_Data_Collection = 2  # "p"- print out the position of the encoder 1
S3_END = 3  # "d"- print out the delta for encoder 1
S4_Stop_Ball_Balancing = 4

# everything that follows will be inside the class
class Task_user:
    """
    @brief Task_user class
    @details This class has a constructor and run() to create a program that allows the user to balance the 
    """
    # definition of constructor

    def __init__(self,enable,Ball_Balanced,printData,state):
        """
        @brief a constructor for Task_user class
        @details This constructor is used to accept two task encoder objects that are passed in from main_lab2.py 
        @details and initialize all the variables needed to successfully implement the data collection and
        @details to facilitate interaction with the encoder objects.
        @param enable Input argument that is used to enable a specified motor.
        @param Ball_Balanced Input argument that is used to enable the ball ballancer mode of the plateform
        @param printData Input argument used to trigger the data collection task that will collect the data of the ball with time.
        @param state Input argument that denotes the state and allows the state to be changed externally to this file.
        """

        ## @brief A variable that represents the current state in the program
        #  @details This variable is used for the system to move on to different states,
        #  @details allowing the user to use desired commands.
        #
        self.state = state

        ## @brief A variable that counts the number of times the user uses the program.
        #  @details Every time the user presses e key command to end the program, this variable is incremented by 1
        #
        self.runs = 0
        
        ## @brief A enable object used to allow a motor to recieve duty.
        #  @details input argument that enables the motor driver via user input during a fualt occurance.
        #
        self.enable = enable
        
        ## @brief A printData object that triggers the printing of the data collected
        #  @details input argument that triggers the printing of the data collected in the data_collection_task.
        #
        self.printData = printData
        
        ## @brief An object that triggers the balancing of the Plateform
        #  @details input argument that triggers the task motor to be enabled allowing duty cycle to the motors of the plateform.
        #
        self.Ball_Balanced = Ball_Balanced


        ## @brief A class object used to trigger ending the program at any time.
        #  @details This object is used to help trigger the action of ending
        #           the program at any point.
        #
        self.endProgram = False
        
    def run(self):
        """
        @brief a function for running the user tasks 
        @details This function uses the Task_encoder_file class objects to implement all the functions created in encoder.py 
        @details whenever the user presses key commands. In addition to the functions in encoder.py, this function allows the user
        @details to implement a data collection interface and to print the data for encoder 1 and encoder 2 positions.
        """

        if(serport.any()):  # waiting for the user key command.
            ## @brief A variable that represents the key command
            #  @details This variable is used to change the state in the program, allowing the user to use desired functions.
            char_in = serport.read(1)
            if(char_in.decode() == 'b' or char_in.decode() == 'B'):
                self.state.put(1)
                pass
            elif(char_in.decode() == 'q' or char_in.decode() == 'Q'):
                self.state.put(3)
                pass
            elif(char_in.decode() == 'g' or char_in.decode() == 'G'):
                self.state.put(2)
                pass
            elif(char_in.decode() == 's' or char_in.decode() == 'S'):
                self.printData.put(True)
                print('ending data collection prematurely')
                pass
            elif(char_in.decode() == 'e' or char_in.decode() == 'E'):
                self.endProgram = True
                pass
            if(self.state.get() == 1):  # set the shaft positions for both encoders to zero
                print('Balancing the panel')
                self.Ball_Balanced = True
                self.state.put(0)
                pass
            if(self.state.get() == 3):  # set the shaft positions for both encoders to zero
                print('Disable Panel Balancing')
                self.Ball_Balanced = False
                self.state.put(0)
                pass
            if(char_in.decode() == 'c' or char_in.decode() == 'C'):
                self.enable.put(True)
                self.duty[0].put(0)
                self.duty[1].put(0)
                pass
        self.runs += 1  # increase the run count by 1
