# -*- coding: utf-8 -*-
"""
Created on Mon Oct 24 12:22:25 2021
@file task_imu.py
@brief A task file to pass variables and calibrate the IMU BNO055.
@details This file contains 2 function files, one to calibrate the IMU BNO055
@details and the run function used to pull euler angles and angular velocity from the IMU driver file.

@author Sangmin Sung
@author Travis Welch
@date November 2, 2021
"""
import utime,pyb,imu_driver_6
from pyb import I2C

class Task_imu:
    ''' 
    @brief IMU Task Class for calibrating and running the BNO055 board.
    @details This has a constructor and 2 function files, one to calibrate the IMU BNO055
             and the run function used to pull euler angles and angular velocity from the IMU driver file.
             The Calibration function when the program first runs, in order to ensure the IMU is working 
             properly before balancing of the ball.
    '''

    def __init__(self,heading,pitch,roll,wx,wy,wz):
        '''
        @brief Constructs the euler angles and angular velocities.
        @details This constructor is used to accept i2c object for instanciating the IMU in master mode 
                 allowing to pull data or send data to the IMU BNO055.
        @param heading The current heading, which correlates to the angle of the plateform with respect to the z-axis.
        @param pitch The current pitch, which correlates to the angle of the plateform with respect to the y-axis.
        @param roll The current roll, which correlates to the angle of the plateform with respect to the x-axis.
        @param wx The angular veloity of the plateform with respect to the x-axis.
        @param wy The angular veloity of the plateform with respect to the y-axis.
        @param wz The angular veloity of the plateform with respect to the z-axis.
        '''
        ## @brief A I2C variable used for the IMU driver.
        #  @details Sets the i2c in master configuration, in order to communicate to and from the IMU.
        #
        self.i2c = I2C(1,pyb.I2C.MASTER)   
        
        ## @brief A imu object used to access imu_driver file.
        #  @details The i2c variable is passed into the imu object to set the attributes needed to use the IMU_driver and the functions within the file.
        #
        self.imu = imu_driver_6.IMU_driver(self.i2c) 
        
        ## @brief One of the Euler angles individualized to Heading.
        #  @details Using the imu_driver file function get_euler to return the current heading, then converting it from byte to decimal number.
        #
        self.heading = heading
        
        ## @brief One of the Euler angles individualized to Pitch.
        #  @details Using the imu_driver file function get_euler to return the current Pitch, then converting it from byte to decimal number.
        #    
        self.pitch = pitch
        
        ## @brief One of the Euler angles individualized to Roll.
        #  @details Using the imu_driver file function get_euler to return the current Roll, then converting it from byte to decimal number.
        #
        self.roll = roll
        
        ## @brief The Angular Velocity of the IMU individualized to Omega X.
        #  @details Using the imu_driver file function get_Velocity to return the current Omega X, then converting it from byte to decimal number.
        #       
        self.wx = wx
        ## @brief The Angular Velocity of the IMU individualized to Omega Y.
        #  @details Using the imu_driver file function get_Velocity to return the current Omega Y, then converting it from byte to decimal number.
        #
        self.wy = wy
            
        ## @brief The Angular Velocity of the IMU individualized to Omega Z.
        #  @details Using the imu_driver file function get_Velocity to return the current Omega Z, then converting it from byte to decimal number.
        #
        self.wz = wz
        
    def calibration(self):
        ''' 
        @brief A function to calibrate the IMU BNO055.
        @details This function serves to properly calibrate the IMU BNO055, and 
                 ensure the gyroscope, magnometer, and accelerometer work properly
                 When the user can orient the IMU in such a way that binary number 
                 reaches 0b11111111, the IMU will be fully calibrated and the coefficients will then be stored.
        '''
        ## @brief Set the operation mode of the imu to nine degree of freedom mode.
        #  @details The 12 correlates to nine degree of freedom mode via the datasheet as 0b1100. This mode will trigger the imu to use the write slaves\
        #           to return the euler angles and angular velocities of the IMU.
        #
        self.imu.run_operation(12)    # Nine Degree of Freedom mode
        
        while(True):
            ## @brief A vairable used to define the current calibration status of the IMU.
            #  @details Once the IMU status returns a 0b11111111 binary number the calbration is complete and ready for use.
            #           The calibration coefficients will then be read and stored into a variable.
            #
            self.cal_status = self.imu.get_IMU_Cal_status()
            if (self.cal_status[0] != 0b11111111):
                print('Please move the IMU until binary number is all = 1, Current Status:', bin(self.cal_status[0]))
                utime.sleep(0.1)
            elif(self.cal_status[0] == 0b11111111):
                self.cal_Coef = self.imu.get_IMU_Cal_Coef()
                print('Fully Calibrated, current status', self.cal_Coef)
                break
    def run(self):
        ''' 
        @brief A function to run the IMU BNO055.
        @details This function serves to pull the current euler angles, and angular velocities
                 from the IMU driver file and make them availabe in the correct units of radians 
                 and radians per second.
        '''
        if(self.cal_status[0] != 0b11111111):
            self.imu.run_operation(0) # configuration mode
            print('Calibrated Coefficients Imported')
            self.imu.set_IMU_Cal_Coef(self.cal_Coef)
            self.imu.run_operation(12) # NDOF 

        h,p,r = self.imu.get_Euler()
        self.heading.put((3.14159/180)*self.imu.convert_byte_to_int(h))
        self.pitch.put((3.14159/180)*self.imu.convert_byte_to_int(p))
        self.roll.put((3.14159/180)*self.imu.convert_byte_to_int(r))
        
        wxx,wyy,wzz = self.imu.get_Velocity()
        self.wx.put((3.14159/180)*self.imu.convert_byte_to_int(wxx))
        self.wy.put((3.14159/180)*self.imu.convert_byte_to_int(wyy))
        self.wz.put((3.14159/180)*self.imu.convert_byte_to_int(wzz))
    