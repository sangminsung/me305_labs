# -*- coding: utf-8 -*-
"""
Created on Tue Sep 21 12:38:41 2021
@file: lab0.py
@author: Sangmin Sung
"""
def fib (idx):
    '''
    @brief This function calculates a Fibonacci number at a specific index.
    @param idx An integer specifying the index of the desired
        Fibonacci number
    '''
    # return 0 for index 0
    if idx ==0:
        return 0
    # return 1 for index 1
    if idx ==1:
        return 1
    f_0 = 0
    f_1 = 1
    # Utilize a for loop to create a fibonacci sequence
    # if statement deals with the f_n = f_(n-1)+f_(n-2) for f_2\
    # else statement deals with the rest of the fibonacci sequence
    for i in range(idx-1):
        if i == 0:
            f_n = f_1+f_0
            f_Minus2 = f_1
        else:
            f_Minus1 = f_n
            f_n = f_Minus1+f_Minus2
            f_Minus2 = f_Minus1 
    return f_n


if __name__ == '__main__':
    # Replace the following statement with the user interface code
    # that will allow testing of your Fibonacci function. Any code
    # within the if __name__ == '__main__' block will only run when
    # the script is executed as a standalone program. If the script
    # is imported as a module the code block will not run.
    ## The index of the Fibonacci number selected by the user
    #Initial index, idx, is intialized to -1 to start looping for user input
    idx = -1
    while idx < 0:
        # type 'e' to terminate the program
        idx = input ('Enter a valid index(enter e for exit): ')
        if idx == 'e':
            break
        elif int(idx) < 0:
            print('Invalid input.')
            idx = int(idx)
            continue
        idx = int(idx)     
        print ('Fibonacci number at ''index {:} is {:}.'.format(idx,fib(idx)))

